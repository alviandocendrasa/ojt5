import java.io.*;
import java.nio.*;
import java.util.stream.*;
import java.util.*;
import java.math.*;
import javax.swing.*;
import java.awt.*;
import java.lang.*;


enum Status {Local, Foreigner};
enum PlanType {Comp1, Comp2};


interface Payable{
	public double getPayment();
}

abstract class Customer implements Payable{
	private static int NO = 1;
	private int id;
	protected String name;
	protected Status status;
	protected PlanType type;
	protected int year;
	
	public Customer(){
		++NO;
		this.id = 1;
		this.name = "Alvin";
		this.status = Status.valueOf("Local");
		this.type = PlanType.valueOf("Comp1");
		this.year = 1;
	}
	
	public Customer(String name, Status status, PlanType type, int year){
		++NO;
		this.id = 1;
		this.name = name;
		this.status = status;
		this.type = type;
		this.year = year;
		
	}
	
	public Customer(Customer c){
		this(c.name, c.status, c.type, c.year);
	}
	
	public String toString(){
		return String.format("Customer id: 000%d%n" + "Name: %s%n" + "No of years: %d", NO, name, year);
	}
	
}



class SimplePlan extends Customer{
	
	public SimplePlan(){
		super();
	}
	
	public SimplePlan(String name, Status s,PlanType type, int years){
		super(name,s,type, years);
	}
	
	public SimplePlan(SimplePlan s){
		this(s.name, s.status,s.type, s.year);
	}
	
	public double getDiscount(){
		if (year > 1){
			return (20.0 * 0.1); 
		}
		
		return 0.0;
	}
	
	
	public double getPayment(){
		return 20 - getDiscount();
	}
	
	public String toString(){
		return String.format("%s%n", super.toString());
	}
}

class ComplexPlan extends Customer{
	protected PlanType p;
	
	public ComplexPlan(){
		super();
		this.p = PlanType.valueOf("Comp1");
	}
	
	public ComplexPlan(String name, Status s,PlanType type, int years){
		super(name,s,type, years);
	}
	
	public ComplexPlan(ComplexPlan g){
		this(g.name, g.status, g.p, g.year);
	}
	
	public double getPayment(){
		if( p == PlanType.valueOf("Comp1")){
			return 50.0;
		}
		
		return 100;
	}
	
	public String getName(){
		return name;
	}
	
	public String toString(){
		return String.format("%s%n" + "Status: %s%n" + "Plan type: %s", super.toString(), status, type);
	}
}

class SeniorPlan extends ComplexPlan implements Serializable{
	private int birth;
	
	public SeniorPlan(){
		super();
		this.birth = 1950;
	}
	
	public SeniorPlan(String name, Status s, PlanType p,int years, int birth){
		super(name,s,p,years);
		this.birth = birth;
		
	}

	
	@Override
	public double getPayment(){
		return super.getPayment();
	}
	
	public int getBirth(){
		return birth;
	}
	
	
	public String toString(){
		return String.format("%s%n" + "year of birth: %d%n", super.toString(),birth);
	}
}


class Alviandocendrasa_Exam_Q1
{
	private static void constructAList(ArrayList<Payable> alist){
		alist.add(new SimplePlan("Heng 1", Status.valueOf("Local"), PlanType.valueOf("Comp2"), 2));
		alist.add(new ComplexPlan("Heng 2", Status.valueOf("Foreigner"), PlanType.valueOf("Comp1"), 3));
		alist.add(new SeniorPlan("Heng 3", Status.valueOf("Foreigner"), PlanType.valueOf("Comp1"), 4, 1930));
	}
	
	private static void displayAlist(ArrayList<Payable> alist){
		for(Payable i: alist){
			if(i instanceof SeniorPlan){
				SeniorPlan obj = (SeniorPlan)(i);
				System.out.printf("%s%n" + "Age: %d year old%n" + "Total payment: %.2f per month%n" + "Customer Type: %s%n", obj.toString(),2021 - obj.getBirth(),  obj.getPayment(), obj.getClass().getName());
				System.out.println("--------------------------------------");
			}
			
			else if (i instanceof ComplexPlan){
				ComplexPlan obj = (ComplexPlan)(i);
				System.out.printf("%s%n%n" + "Total payment: %.2f per month%n" + "Customer Tpye: %s%n",obj.toString(), obj.getPayment(), obj.getClass().getName());
				System.out.println("--------------------------------------");
			}
			
			else if (i instanceof SimplePlan){
				SimplePlan obj = (SimplePlan)(i);
				System.out.printf("%s%n" + "Total payment: %.2f per month%n" + "Customer Tpye: %s%n",obj.toString(), obj.getPayment(), obj.getClass().getName());
				System.out.println("--------------------------------------");
			}
		}
	}
	
	private static void processSilver(ArrayList<Payable> alist){
		
		System.out.println("Our seniors who received awards");
		for(Payable i: alist){
			if(i instanceof SeniorPlan){
				SeniorPlan obj = (SeniorPlan)(i);
				System.out.printf("Name: %s       Age = %d%n", obj.getName(), 2021-obj.getBirth());
			}
		}
	}
	
	public static void main(String[] args) throws Exception
	{
		ArrayList<Payable> alist = new ArrayList<Payable>();
		constructAList(alist);
		displayAlist(alist);
		System.out.println();
		
		processSilver(alist);
	}
	
	
	
	
}
