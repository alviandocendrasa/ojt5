import java.util.Scanner;
import java.util.ArrayList;
import java.util.Random;


class Number
{
	private int n;
	
	public Number(int n){
		this.n = n;
	}
	
	public String guessANumber(int m){
		String result = "";
		String answer = String.valueOf(m);
		String user_answer = String.valueOf(n);
		
		for(int i = 0; i < 5; i++){
			if(answer.charAt(i) == user_answer.charAt(i)){
				result += "O";
			}
			
			else if(answer.charAt(i) > user_answer.charAt(i)){
				result += "H";
			}
			
			else if(answer.charAt(i) < user_answer.charAt(i)){
				result += "L";
			}
		}
		
		return result;
		
	}
}

class Alviandocendrasa_E3
{
	
	private static Scanner input;

	public static void main(String [] args)
	{
		input = new Scanner(System.in);
		Random rd = new Random();
		
		System.out.println("I have 5 digits integer");
		System.out.println("Guess what is this integer");
		System.out.println();
		
		int number = Math.abs(rd.nextInt() % 100000);
		
		for(int i = 1; i < 6; i ++){
			System.out.print("Your guess: ");
			Number num_input = new Number(input.nextInt());
			
			System.out.printf("Analysis: %s%n", num_input.guessANumber(number));
			
			if(num_input.guessANumber(number) == "OOOOO"){
				System.out.println("Bravo!! You got it");
			}
			
			else{
				System.out.println("Wrong guess, try again");
			}
			
			System.out.println("----------------------------");
			
		}
	}
	
	
}